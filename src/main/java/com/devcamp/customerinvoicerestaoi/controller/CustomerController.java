package com.devcamp.customerinvoicerestaoi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoicerestaoi.model.Customer;
import com.devcamp.customerinvoicerestaoi.service.CustomerService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> AllCustomer = customerService.getAllCustomer();
        return AllCustomer;
    }

}
