package com.devcamp.customerinvoicerestaoi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoicerestaoi.model.Invoice;
import com.devcamp.customerinvoicerestaoi.service.InvoiceService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @GetMapping("/invoices")
    public ArrayList<Invoice> getAllInvoices() {
        ArrayList<Invoice> AllInvoice = invoiceService.getAllInvoice();
        return AllInvoice;
    }

    @GetMapping("/invoices/{invoiceId}")
    public Invoice getInvoiceByIndex(@PathVariable(required = true) int invoiceId) {
        ArrayList<Invoice> getAllInvoice = invoiceService.getAllInvoice();
        Invoice findInvoice = new Invoice();
        if (invoiceId < getAllInvoice.size()) {
            findInvoice = getAllInvoice.get(invoiceId);
        }
        return findInvoice;

    }

}
