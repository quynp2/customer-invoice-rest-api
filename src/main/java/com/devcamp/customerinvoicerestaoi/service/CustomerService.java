package com.devcamp.customerinvoicerestaoi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customerinvoicerestaoi.model.Customer;

@Service
public class CustomerService {

    Customer KhachHang001 = new Customer(001, "Ho Xuan Tien", 30);
    Customer KhachHang002 = new Customer(002, "Ho Ha An", 10);
    Customer KhachHang003 = new Customer(003, "Tran  Nhan", 15);
    Customer KhachHang004 = new Customer(004, "Tran Ngoc ", 20);
    Customer KhachHang005 = new Customer(005, "Nhan Tran Ngoc ", 5);
    Customer KhachHang006 = new Customer(006, "Tran Ngoc Nhan Pham", 10);

    public ArrayList<Customer> getAllCustomer() {
        ArrayList<Customer> AllCustomer = new ArrayList<>();
        AllCustomer.add(KhachHang001);
        AllCustomer.add(KhachHang002);
        AllCustomer.add(KhachHang003);
        AllCustomer.add(KhachHang004);
        AllCustomer.add(KhachHang005);
        AllCustomer.add(KhachHang006);
        return AllCustomer;

    }

}
