package com.devcamp.customerinvoicerestaoi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.customerinvoicerestaoi.model.Invoice;

@Service
public class InvoiceService {
    @Autowired
    private CustomerService customerService;

    Invoice Invoice01 = new Invoice(10102, 3750000);
    Invoice Invoice02 = new Invoice(101234, 345000);
    Invoice Invoice03 = new Invoice(3444, 37500);
    Invoice Invoice04 = new Invoice(5555, 12000);
    Invoice Invoice05 = new Invoice(67777, 34000);
    Invoice Invoice06 = new Invoice(678884, 2357600);
    Invoice Invoice07 = new Invoice(12, 760000);
    Invoice Invoice08 = new Invoice(233, 4500000);
    Invoice Invoice09 = new Invoice(4566, 34590000);

    public ArrayList<Invoice> getAllInvoice() {
        ArrayList<Invoice> AllInvoice = new ArrayList<>();
        Invoice01.setCustomer(customerService.KhachHang002);
        Invoice02.setCustomer(customerService.KhachHang001);
        Invoice03.setCustomer(customerService.KhachHang004);
        Invoice04.setCustomer(customerService.KhachHang005);
        Invoice05.setCustomer(customerService.KhachHang003);
        Invoice06.setCustomer(customerService.KhachHang006);
        Invoice07.setCustomer(customerService.KhachHang002);
        Invoice08.setCustomer(customerService.KhachHang005);
        Invoice09.setCustomer(customerService.KhachHang003);

        AllInvoice.add(Invoice01);
        AllInvoice.add(Invoice02);
        AllInvoice.add(Invoice03);
        AllInvoice.add(Invoice04);
        AllInvoice.add(Invoice05);
        AllInvoice.add(Invoice06);
        AllInvoice.add(Invoice07);
        AllInvoice.add(Invoice08);
        AllInvoice.add(Invoice09);

        return AllInvoice;

    }

}
